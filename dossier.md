# PRODUCTIEDOSSIER EXODUS #
## Functionele specificaties ##

* One Page Webapplication
  * Bookmark routes
* Preloader/loader
  * Terwijl de JSON(P) inlaadt
  * Animatie via SVG
* De meeste inhoud wordt beheerd in databestanden en dynamisch ingeladen
* Adaptive images
* Google Maps integratie
* Lokaal caching van data en bronbestanden
* Gebruiker ervaart een interactieve webapplicatie
* Gebruiker kan de webapplicatie bookmarken in browser, bureablad en als native app in het overzicht
* Responsive design
* Automation
  * Bower
  * Grunt
  
## Technische specificaties ##

* Core technologies
  * HTML5
  * CSS3
  * JavaScript
* Template engine
  * Handlebars
* Storage
  * LocalStorage
* Bibliotheken
  * JQuery
  * lodash
  * crossroads.js
  * js-signals
  * hasher.js

## Persona's ##

![First persona](images/persona.png)
![Second persona](images/persona2.png)
![Third persona](images/persona3.png)

## Ideeënborden ##

![Buttons](images/ideaboards.png)
![Colors](images/ideaboards2.png)
![Colors](images/ideaboards3.png)
![Icons](images/ideaboards4.png)
![Icons](images/ideaboards5.png)
![Fonts](images/ideaboards6.png)
![Fonts](images/ideaboards7.png)
![Fonts](images/ideaboards8.png)
![Font combinations](images/ideaboards9.png)
![Font combinations](images/ideaboards10.png)
![Photography](images/ideaboards11.png)
![UI Examples](images/ideaboards12.png)
![UI Examples](images/ideaboards13.png)

## Moodboards ##

![First moodboard](images/moodboards.png)
![Second moodboard](images/moodboards2.png)
![Third moodboard](images/moodboards3.png)
![Fourth moodboard](images/moodboards4.png)

### Finaal moodboard ###

![Final moodboard](images/moodboards5.png)

## Sitemap ##

![Sitemap](images/sitemap.png)

## Wireframes ##

![Wireframe](images/wireframes.png)
![Wireframes](images/wireframes2.png)
![Wireframe](images/wireframes3.png)
![Wireframe](images/wireframes4.png)

## Style tiles ##

![First style tile](images/style_tiles.png)
![Second style tile](images/style_tiles2.png)
![Third style tile](images/style_tiles3.png)

### Finale style tile ###

![Final style tile](images/style_tiles4.png)

## Visuals ##
### Mobile ###
![Home](images/sd1_m.png)
![Disclaimer](images/sd2_m.png)
![Question](images/sd3_m.png)
![Results](images/sd4_m.png)
![Country](images/sd5_m.png)

### Desktop ###
![Home](images/sd1_m.png)
![Question](images/sd3_m.png)
![Results](images/sd4_m.png)
![Country](images/sd5_m.png)

## Screenshots eindresultaat ##
### Screenshots for 7 breakpoints ###
![320px](screenshot_320.png)
![480px](screenshot_480.png)
![640px](screenshot_640.png)
![800px](screenshot_800.png)
![960px](screenshot_960.png)
![1024px](screenshot_1024.png)
![1280px](screenshot_1280.png)

### Mobile screenshots ###
![Home](images/screenshot_home_m.png)
![Question](images/screenshot_question_m.png)
![Results](images/screenshot_results_m.png)
![Country page](images/screenshot_detail_m.png)

### Desktop screenshots ###
![Home](images/screenshot_home.png)
![Disclaimer](images/screenshot_disclaimer.png)
![Question](images/screenshot_question.png)
![Results](images/screenshot_results.png)
![Country page](images/screenshot_detail.png)

### Screencast ###

[Click here for MPEG file](screencast.mpeg)

## Screenshot snippets ##

### HTML ###
![Snippet HTML](images/snippet_html.png)

## CSS ##
![Snippet CSS](images/snippet_css.png)

## JavaScript ##
![Snippet JS](images/snippet_js.png)
![Snippet JS](images/snippet_js2.png)
![Snippet JS](images/snippet_js3.png)

## Tijdsbesteding per student ##

* [Klik hier voor het Excel-bestand](timesheet.xlsx)