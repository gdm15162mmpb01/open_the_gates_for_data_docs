# EXODUS #

This is the BitBucket repository for our Exodus-application. The goal is to make it easy for refugees (and other people who'd like to move cross border) to find a country that suits him/her the best.

### What does the application do? ###

* It asks you 16 questions
* It will give a top 5 of countries which suit you the best
* It will show you some basic information about the country and a map

### Where can I find the production files? ###

* [Here](dossier.md)

### Who made this possible? ###

* Bram De Backer
* Zeger Caes
* Feline Dekeyser